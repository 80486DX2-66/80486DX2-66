# Preferred file sharing services

Source: https://docs.google.com/spreadsheets/d/1vrKixs%5FItQlLnGK6%5FD22qP3NhRPiD8n7SATX81CGzzs/view

Status: up, HTTPS: yes, CDN: not Cloudflare → resulted in no CDNs, logging: no:
- https://cockfile.com : 128 MiB limit, temporary hosting: 12 hours
- https://uguu.se : 100 MB limit, no scanning, temporary hosting: 24 hours

Other sources:
- https://qu.ax : 256 MB limit, no temporary hosting
