# Privacy recommendations

> **Note**
> Copied from a group of DMs written by me, and reformatted.

Not a security specialist though. Please check out https://www.privacyguides.org and https://www.privacytools.io

Operating system (home use):
- Artix Linux (Artix Linux ≈ Arch Linux without systemd)
- Trisquel Mini (Trisquel Mini must be as FOSS as possible)
- OpenBSD (I'd recommend OpenBSD as a secure and well-documented OS)

Operating system (live):
- Tails
- Linux Kodachi

Browser:
- LibreWolf
- Mozilla Firefox downloaded from [the FTP server](https://ftp.mozilla.org/pub/firefox/releases/) or built by someone FOSS (so it would be without tracking ID Mozilla now adds) **and** with [Arkenfox's user.js](https://github.com/arkenfox/user.js)

For your smartphone: Assuming the firmware is proprietary, you could be tracked / exposed in other ways. As for connecting your phone to the VPN, perhaps [this](https://support.google.com/android/answer/9089766) would help.

Recommended Android firmware:
- DivestOS (bonus: [F(L)OSS Android apps by DivestOS](https://divestos.org/pages/our_apps))
- GrapheneOS
- see more at https://www.privacyguides.org/en/android/

Tests of Android browsers for privacy: https://privacytests.org/android.html

The best VPN must be Mullvad.

When using Internet without/with a VPN, use DNS over HTTPS (I don't think Cloudflare as provider would be good enough, but you might like it). Firefox and its forks (LibreWolf, Waterfox) have great support for the technology.

General recommendations:
- Intel CPUs: Clean your Intel ME/TXE of spyware with https://github.com/corna/me%5Fcleaner
- AMD CPUs: *crickets*
- BIOS: SeaBIOS
- EFI/UEFI: Libreboot
- Protect your computer case and display from radiating EMI which can be used for Tempest/Van Eck phreaking. Protect your input devices by adding ferrite bands on their cables if they don't have them. Mostly devices are FCC-compliant nowadays.
