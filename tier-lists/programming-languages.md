# Programming languages tier list (subjective)

Ready: 85%

| Mark | Languages |
| :-- | :-- |
| S | D, Go, Haskell, Perl, Python |
| A | Apache Groovy, C, Crystal, Dart, Elixir, Julia, Prolog, Ruby, Scala, Scheme |
| B | ActionScript, Common Lisp, Java, Kotlin, Lisp, Lua, Swift, TypeScript, V |
| C | Ada, C#, C++, Clojure, Delphi, Erlang, F, F#, Hack, JavaScript, Rust |
| D | SAP ABAP, Cobol, Objective-C, Pascal (FreePascal), PHP, MS Visual Basic |
