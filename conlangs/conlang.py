#!/usr/bin/env python3

# conlang.py
# version 2
#
# Author: Intel A80486DX2-66
# License: Creative Commons Zero 1.0 Universal or Unlicense
# SPDX-License-Identifier: CC0-1.0 OR Unlicense

from json import load as json_load
from os.path import basename
from sys import argv, exit

HELP_ARGS = ["-h", "--help"]

is_attrib = lambda k: k.startswith("@")

if __name__ == "__main__":
    if len(argv) < 2 or (len(argv) > 1 and argv[-1] in HELP_ARGS):
        print(f"Usage: {basename(argv[0])} <path to JSON file>")
        exit(1)

    # Load the JSON data from the file path specified in argv[1]
    file_path = argv[1]
    with open(file_path, 'r') as file:
        data = json_load(file)

    # Display the name
    print(f"Constructive language {data.get('@name', argv[1])}\n")

    # Value separator
    value_separator = data.get("@separator", ' ')

    # Automatic order
    if "@order" not in data:
        data["@order"] = {k: v for k, v in data.items() if not is_attrib(k)}

    # Display information with titles based on the JSON data
    for item in data["@order"]:
        if item not in data:
            raise ValueError(f"Key '{item}' doesn't exist as specified in the "
                             "order")

        info = data[item]

        is_dict = isinstance(info, dict)

        print_end = "\n" if is_dict else " "
        print(f"{item}:", end=print_end)

        if isinstance(info, list):
            print(value_separator.join([f"{letter}" for letter in info]))
        elif is_dict:
            info_type = info.get("@type", "enum")
            if info_type == "enum":
                list_separator = info.get("separator", ' ')
                for key, value in info.items():
                    if is_attrib(key):
                        continue

                    print(f"* {key} -> ", end="")
                    if isinstance(value, int) or isinstance(value, str):
                        print(value)
                    elif isinstance(value, list):
                        print(list_separator.join(value))
                    elif isinstance(value, dict):
                        dict_separator = value.get("separator", ', ')
                        print(dict_separator.join([f"{k}: {v}" for k, v in
                                                   value.items()]))
                    else:
                        raise ValueError("Unknown type "
                                         f"'{type(value).__name__}' of '{key}' "
                                         f"in enum '{item}'")
            else:
                raise ValueError(f"Unknown info type '{type(info).__name__}'")
