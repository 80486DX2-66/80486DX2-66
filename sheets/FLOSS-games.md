# Free, Libre, Open-Source Games

> **Note**
> This document is Work In Progress

> **Note**
> Thanks to Gersonzao for the rest of the URLs!

Sorted alphabetically in ascending order:

- [Category "Game" in the software directory of Free Software Foundation][FSF]
- [LibreGameWiki][LibreGameWiki]
- [Open Source Game Clones][osgameclones]
- [Open source games list (OSGL) at trilarion.github.io][trilarion]
- [Unix ASCII games][awesome-ttygames]

[FSF]: https://directory.fsf.org/wiki/Category/Game
[LibreGameWiki]: https://libregamewiki.org/Main%5FPage
[awesome-ttygames]: https://github.com/ligurio/awesome-ttygames
[osgameclones]: https://osgameclones.com
[trilarion]: https://trilarion.github.io/opensourcegames/

<!-- DRAFT

https://forum.freegamedev.net/index.php

-->
