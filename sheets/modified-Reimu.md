# Modified Reimu

This document uses standard arithmetic notation.

The assertions are evaluated in linear order, from the first to the last.

1. Let my wife be Reimu Hakurei
2. Let my age be `a`, and my wife's age be `b`, then `a - b < 4`
3. Let my height (cm) be `a`, and my wife's height (cm) be `b`, then
   `a - b < 10`
4. Let my wife be a perfect wife, then my wife doesn't have unhealthy habits
5. Let my wife be a perfect wife, and I mustn't care about how others see our
   relationship, then as a husband I don't have to love her particular parts to
   love her as a whole
6. Let my wife be a perfect wife, then her every embrace is heavenly to me as a
   husband
