Scarlet Devil Mansion Security System
=====================================

| Field              | Value                               |
| :----------------- | :---------------------------------- |
| Technical Name     | SDMSEC-1.0                          |
| Authors            | Larry Holst                         |
| Reviewed By        | Remilia Scarlet, Sakuya Izayoi      |
| License            | Creative Commons Zero 1.0 Universal |
| Revision           | 1.0                                 |
| Last Revision Date | 2025-02-15                          |
| Format             | Human-readable Markdown document    |

********************************************************************************

1 Foreword
----------

This standard specifies the requirements for the Scarlet Devil Mansion Security
System.

The author, Larry Holst, was visiting the Scarlet Devil Mansion and noticed the
lack of comprehensive security measures beyond reliance on magic powers. As an
atheist, the author considered that magical abilities alone are insufficient
and cannot be certified by current security standards or automated. Recognizing
the need for a more robust and technologically-driven security system, the
author set out to develop a standard that would address the unique requirements
of the Scarlet Devil Mansion and provide a reliable, configurable, and
tamper-resistant solution to protect the premises and its inhabitants.


2 Introduction
--------------

This standard outlines the requirements for the security system implemented at
the Scarlet Devil Mansion.


3 Scope
-------

This standard applies to the security system installed at the Scarlet Devil
Mansion in Gensokyo.


4 Normative References
----------------------

The following documents are referenced in this standard:
* SvarDOS (forked to create ScarletDOS)
* AES-128-CBC encryption standard
* AES-256-CBC encryption standard


5 Terms and Definitions
-----------------------

For the purposes of this standard, the following terms and definitions apply:
* AI (Artificial Intelligence): A system of one or more neural networks or
  other machine learning models designed to perform specific tasks, such as
  intruder detection and classification, without requiring explicit
  programming.
* BIOS (Basic Input/Output System): The firmware responsible for performing
  hardware initialization and providing a standard interface for accessing
  low-level hardware components in a computer system.
* COM (Communication Port): A serial communication interface, typically using a
  DE-9 connector, used for data exchange between the security system and other
  devices
* Camera: A device used to capture visual information and transmit it to the
  security system for monitoring and recording purposes.
* Clock Synchronization: The process of ensuring that the internal clocks of
  various system components, such as the main computer and microcontrollers,
  are synchronized to maintain consistent time across the entire security
  system.
* DMA (Direct Memory Access): A method of transferring data between system
  memory and peripheral devices without the direct involvement of the central
  processing unit.
* Electric Fence: A physical barrier surrounding the Scarlet Devil Mansion
  property, designed to deter and detect unauthorized entry.
* Fork (Software Fork): A copy of a software project that is developed
  independently, typically to introduce new features or modifications not
  present in the original project.
* Humidity Sensor: A device used to measure the amount of water vapor present
  in the air within the Scarlet Devil Mansion.
* Microcontroller: A compact, single-chip computer used to control specific
  functions, such as the operation of doors, windows, and gates, within the
  security system.
* Offline: A state where a system component, such as a device or sensor, is
  temporarily disconnected from the main security system and unable to
  communicate or exchange data.
* Online: A state where a system component, such as a device or sensor, is
  connected to the main security system and able to communicate and exchange
  data.
* Rechargeable Battery: A power source that can be recharged and used to
  provide backup power to the microcontrollers and low-power AI components of
  the security system.
* Shielding of Wiring: The use of specialized materials and techniques to
  protect electrical wiring from electromagnetic interference and other
  environmental factors that could disrupt the proper functioning of the
  security system.
* Temperature Sensor: A device used to measure the ambient temperature within
  the Scarlet Devil Mansion.
* UPS (Uninterruptible Power Supply): A backup power source that provides
  continuous electricity to the main computer in the event of a power outage or
  disruption, ensuring the continued operation of the security system.


6 System Overview
-----------------

The Scarlet Devil Mansion Security System consists of a middle-sized computer
with custom BIOS, microcontrollers for controlling doors, windows, and gates,
temperature and humidity sensors installed throughout the mansion, and an
electric fence surrounding the property. Additionally, there is a UPS
(Uninterruptible Power Supply) for the main computer and a rechargeable battery
for the microcontrollers and low-power AI.


7 System Requirements
---------------------

The system shall meet the following requirements:
* The computer shall run on ScarletDOS, a fork of SvarDOS.
* The system shall feature an AI-powered intruder alert system, which is
  specified in Section 7a, AI Specifications.
* The system shall have a wireless interface for camera monitoring, using the
  encryption methods detailed in Section 12, Encryption Details.
* The system shall automatically lock windows, doors, and gates upon detecting
  intruders, with the ability to disable some locks through the main computer
  BIOS's settings.
* The system shall be able to activate or deactivate the electric fence on a
  scheduled basis or as needed.


7a AI Specifications
--------------------

The AI-powered intruder alert system shall have the following specifications:
* The AI shall be capable of detecting and classifying intruders with high
  accuracy.
* The AI shall be able to differentiate between authorized personnel and
  intruders.
* The AI shall be able to predict the movement and intentions of detected
  intruders.

The AI-powered intruder alert system shall:
* Run on a dedicated low-power, ecological single-board computer, certified to
  maintain data security and avoid bit flips.
* Be capable of detecting and classifying intruders with high accuracy.
* Be able to differentiate between authorized personnel and intruders.
* Be able to predict the movement and intentions of detected intruders.
* Be capable of leveraging additional computational resources, such as a server
  or another computer, to process events at a faster rate or as a backup, as
  configured by the mansion's owner.


7b Escape Route
---------------

The system shall provide emergency escape routes for all residents of the
mansion:
* The escape routes shall be physically secured using specialized materials to
  prevent unauthorized access.
* The escape routes may lead to various locations, such as Cirno's House or the
  Hakurei Shrine, as configured by the mansion's owner.
* The escape route configuration, including the destination locations and
  physical security measures, shall be customizable by the mansion's owner.


8 Operational Procedures
------------------------

The system shall be operated in accordance with the following procedures:
* The computer shall continuously monitor all systems in real-time.
* The system shall be controlled via the computer's operating system
  ScarletDOS.
* The system shall manage the key rotation mechanism for the wireless
  interface.
* When a device reconnects after being offline, users shall verify new
  encryption keys visually and by text if the device missed a key rotation, to
  ensure the device is using the current encryption key.
* The system settings, including clock synchronization, network access control,
  backup power source, and escape route configuration, can be customized
  through the computer's BIOS.


9 Emergency Protocols
---------------------

In the event of an intruder alert, the system shall:
* Lock all entry points, including doors, windows, and gates.
* Activate the electric fence.
* Notify designated personnel via their devices.
* Ensure continued operation during power outages using the UPS for the main
  computer and the rechargeable battery for the microcontrollers and low-power
  AI.


10 User Instructions
--------------------

Designated personnel is responsible for:
* Monitoring devices and ensuring they are charged daily when power is
  available.
* Performing regular checks of the system settings to ensure optimal
  performance.


11 Maintenance
--------------

The system shall be maintained in accordance with the following requirements:
* All wiring shall be shielded to prevent interference.
* Regular updates to the software and firmware shall be performed.
* All sensors and cameras shall be functioning correctly.


12 Encryption Details
---------------------

The system shall use the following encryption methods:
* AES-128-CBC for camera data.
* AES-256-CBC for intruder detection signals.


Annex A (Informative) - System Diagram
--------------------------------------

The system diagram is shown in the file `system_diagram.svg` included in this
standard.


Frequently Asked Questions
--------------------------

**Q: Why did you choose to develop a custom operating system, ScarletDOS,
instead of using an existing OS like Linux or Windows?**

A: The decision to develop a custom operating system, ScarletDOS, was made to
ensure the highest level of control and customization over the security
system's core functionality. Well-established operating systems like Linux and
Windows, while powerful and feature-rich, can be difficult to fully audit and
verify for security vulnerabilities, especially when dealing with
mission-critical applications like a mansion security system.

ScarletDOS, being a fork of the SvarDOS project, allows us to closely inspect
the codebase, implement specialized security measures, and optimize the system
for the specific requirements of the Scarlet Devil Mansion. This level of
control and transparency is crucial for a security-focused application where
the safety and privacy of the residents are of the utmost importance.

**Q: Why did you choose to run the AI-powered intruder alert system on a
separate low-power computer?**

A: The decision to run the AI-powered intruder alert system on a dedicated
low-power computer was made to ensure the reliability and responsiveness of the
security system as a whole. By offloading the computationally intensive AI
tasks to a separate, specialized hardware component, we can ensure that the
main computer remains focused on its core responsibilities, such as managing
the physical security controls, monitoring sensors, and coordinating the
overall system.

This architectural approach also allows for greater flexibility and
scalability. The low-power AI computer can be easily upgraded or replaced
without disrupting the rest of the security system, and it can leverage
additional computational resources, such as a server or another computer, to
process events at a faster rate or as a backup, as configured by the mansion's
owner.

**Q: Why not use Microsoft Windows for the security system?**

A: While Microsoft Windows is a widely-used and feature-rich operating system,
its closed-source nature and the complexity of its codebase make it challenging
to thoroughly audit and verify for security vulnerabilities. As a
security-critical application, the Scarlet Devil Mansion Security System
requires a level of transparency and control that is difficult to achieve with
proprietary operating systems like Windows.

Additionally, the legal restrictions and licensing requirements associated with
Microsoft Windows make it less suitable for a custom, mission-critical
application like this security system. The ability to freely inspect, modify,
and distribute the underlying codebase is essential for ensuring the system's
reliability, security, and long-term maintainability.
