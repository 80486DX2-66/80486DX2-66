## Legal Disclaimer

[東方Project][Touhou Project] (Tōhō Project) is a series of bullet hell shooter
games created by Jun'ya Ota (ZUN), and all characters and settings are the
intellectual property of Team Shanghai Alice. This project is a fan work and is
not affiliated with or endorsed by ZUN or Team Shanghai Alice.

The [Scarlet Devil Mansion][Scarlet Devil Mansion] Security System standard is
based on the fictional setting of the 東方Project universe, created by Jun'ya
Ota (also known as ZUN).  The Scarlet Devil Mansion and the land of Gensokyo
are not real-world locations, but rather elements of a fantasy world depicted
in the 東方Project games and related media.

This standard is intended for creative and entertainment purposes only, and
does not reflect or represent any actual security systems or real-world
locations. The information and specifications provided in this standard are
purely fictional and should not be interpreted as factual or applicable to any
real-world scenarios.

The authors of this standard do not claim any ownership or rights over the
東方Project intellectual property. All rights and trademarks related to
東方Project belong to Jun'ya Ota and any other relevant copyright holders.

This standard is released under the Creative Commons Zero 1.0 Universal (CC0
1.0) license, which means it is dedicated to the public domain. Anyone is free
to use, modify, and distribute the content of this standard for any purpose,
without any restrictions or requirements for attribution.

[Scarlet Devil Mansion]: https://en.touhouwiki.net/wiki/Scarlet_Devil_Mansion
[Touhou Project]: https://en.touhouwiki.net/wiki/Touhou_Project
