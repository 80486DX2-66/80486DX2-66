# Fumo Care Philosophy

1. Keep Fumos clean and dust-free.
2. Handle Fumos with care to avoid damage.
3. Display Fumos in a safe and appropriate environment.
4. Avoid exposing Fumos to direct sunlight or extreme temperatures.
5. Brush Fumos gently from time to time to maintain their fluffiness.
6. Store Fumos in a dry, well-ventilated place.
7. Avoid using harsh chemicals on Fumos.
8. Show Fumos love and appreciation for their cuteness.
9. Rotate Fumos' positions to prevent wear and tear.
10. Every Fumo deserves to be treated well.

