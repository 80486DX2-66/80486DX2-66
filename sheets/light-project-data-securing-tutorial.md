> **Note**
> The markup of this Markdown-formatted document has been modified to make the graphical information visible immediately here and to remove unnecessary blank lines.

Securely storing passwords is a fundamental concept in computer security- oH **knock** **knock** FBI OPEN UP!

You're now arrested (for whatever reason)... now what?

Whether they are FBI, or just regular police, or an entirely different agency,

The very first thing they will do is seize all your electronic equipment.. Things like mobile phones, laptops and even desktop computers. Even your smart watch.

Their objective by seizing your equipment, they will be able to search them for evidence for the crime they suspect you of (or) additional crimes or associates they may been previously unaware of.

Having encrypted your electronic devices is not always the case in the real world, however, even if your device is encrypted by a really long, truly random passphrase, they can still crack it. Real world examples such as this French activist prove this: https://onniforums.com/thread-3181.html

However, do not panic! Remember: security is done through layers, if one layer is broken, another shall stop the attack. Device encryption in this context being merely a layer. Never bet your freedom on 1 security layer.

There are also other scenarios where encrypting your device is simply not optimal due to the real risk of torture, i.e. you live in a third-world country and you are an activist against the government. This xkcd perfectly explains the situation:

![](https://imgs.xkcd.com/comics/security.png)

So, your next layer for protecting your data, especially your passwords is through obscurity

We at Light & Secure Project like to think of obscurity as just another layer for security. It on own cannot replace other layers, but remember, security works through layers! If one security layer has been somehow passed, the other layer should stop the attack and so on and so fourth.

As introduced in the start of the thread, passwords are fundamental to any security model. If your adversary gets their hand on your password database, or even know it is there, it can mean even more trouble for you..

Here is a simple solution using stegnography tools I've recently released under "The Light & Secure Software Project"....

Here is an example, step-by-step ways to hide files, especially your password database:

Step 1.
Create a new folder somewhere, not necessarily somewhere "secret". If you are on Windows or Linux, the home directory folder `Pictures` is a good place for this.

Download many .PNG pictures in this folder, as many as you could. Preferably the pictures should be of safe-for-work, innocent nature.

If your job involves dealing or caring for animals, or if you have a pet animal, I would recommend picking animal pictures. And so on. This provides some light form of "plausible deniability" as not many will suspect there is something odd involving those pictures.

I recommend having over 1000 .PNG pictures in the folder.
They must be .png pictures since the software we will use only works with .PNG pictures.
I recommend the pictures to have high-entropy. As in, avoid pictures with just plain black backgrounds and nothing else, the more intersecting colors, the better.  Think of a dog getting abducted by alien space ship and some missile launched at the ship that leaves traces of a rainbow trail behind it while the sun is setting.

Step 2.
Download and install Python, and download Caesar13 here: https://codeberg.org/Light-Project/caesar13
Caesar13 is a stegnography tool written in Python3 which allows you to hide files in .png images.

The usage manual is on the repository page. It's pretty straight forward.

Step 3.
Download secpass here: https://codeberg.org/Light-Project/secpass
Secpass is a minimal, password manager, which produces password database files with no file signature, and thus is unrecoverable (and undetectable) by forensics tools, if the file is deleted.


Step 4.
Create new password database file with secpass, here you can store passwords safely. Save it to a file, call it anything.

Pick a random picture, memorize the file name. Then use Caesar13 to hide the file into the picture. Pick a strong password in Caesar13 and memorize it.

So in technicality; you have 3 passwords. 1st is the picture file name, 2nd is caesar13 key, and 3rd (and last) is secpass database file master password

When you wish to view, delete, or add passwords to the password manager secpass, you extract the database file using caesar13, open it with secpass, do what you need to do, then delete it. And since secpass database file leaves no traces on disk if deleted, forensics won't be able to tell there was a password file, let alone a deleted one.

There may be other artifices left, however, those are filesystem dependent and I will release a tutorial soon covering those.

For this guide, the previously mentioned ways will work wonders. And may be the difference of freedom, or a long prison sentence.
