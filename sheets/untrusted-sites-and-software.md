[origin_URL]: https://www.reddit.com/r/FREEMEDIAHECKYEAH/comments/10bh0h9/unsafe_sites_software_thread/

Origin: [r/FREEMEDIAHECKYEAH][origin_URL]. This document is a modification of
the original post.

# Untrusted Sites / Software

## Game Sites

> **Note**
> Scene groups do not have their own sites, so it's best to avoid anything
> claiming to represent one.

- OceanOfGames - Caught with malware multiple times
- CrackingPatching - Caught with [malware][CrackingPatching]
- IGG / LoadGames - Added DRM to uploads, doxxed member
- AGFY - Malicious redirect ads
- AimHaven - Malicious redirect ads
- SteamUnlocked - Malicious redirect ads, slow file host, IGG / nosteam uploads
- GoGUnlocked / ROMsUnlocked - SteamUnlocked sister sites
- xGIROx - Caught with malware
- BBRepacks - Caught with malware
- Seyter / Qoob - Caught with malware, tried to [switch names][qoobrepacker]
- cracked-games - Caught with malware
- Wifi4Games - Caught with malware
- GameFabrique - IGG uploads + [adware installers][GameFabrique]
- Fake FitGirl Sites:
  ```text
  real: fitgirl-repacks.site
  fake: fitgirl-repack.site
  ```

[CrackingPatching]: https://redd.it/qy6z3c
[qoobrepacker]: https://rentry.co/qoobrepacker
[GameFabrique]: https://rentry.co/GameFabrique_Adware

## Software / App Sites

> **Note**
> YouTube videos claiming to give away free software are nearly always scams.

- GetIntoPC / GetIntoMAC - Caught with malware multiple times
- SadeemPC / SadeemAPK - Caught with malware multiple times
- KaranPC - Caught with malware multiple times
- AliTPB / AliPak / b4tman - Caught with malware multiple times
- FTUApps - Caught with [malware][FTUApps]
- S0ft4PC / Portable4PC - Caught with malware
- CracksHash - Caught with [malware][CracksHash]
- haxNode - Caught with malware
- IGI30 - Caught with malware
- MainRepo / MRepo - Caught with [malware][MainRepo MRepo]
- AppValley / TutuBox / Ignition - History of
  [DDoS attacks][AppValley TutuBox Ignition]
- CNET / Download.com / ZDNET - History of [adware][CNET Download.com ZDNET]

[FTUApps]: https://redd.it/120xk62
[CracksHash]: https://redd.it/lklst7
[MainRepo_MRepo]: https://rentry.co/zu3i6
[AppValley TutuBox Ignition]: https://github.com/nbats/FMHYedit/pull/307
[CNET Download.com ZDNET]: https://www.reddit.com/r/software/comments/9s7wyb/whats_the_deal_with_sites_like_cnet_softonic_and/e8mtye9/

## Torrent Sites / Clients

> **Note**
> Some aggregators search sites like TPB, so it's recommended to avoid using
> them for software and games.

- Kickass Torrents - Official site is long gone, all that remain are sketchy
  copycats
- The Pirate Bay / TPB - Site is no longer moderated, so its very risky for
  software and games
- VSTorrent - Caught with [malware][VSTorrent]
- uTorrent - Considered [adware][uTorrent], pre-adware versions exist, but it's
  best to use [open source clients][open source clients]
- BitTorrent / BitComet - Adware
- Frostwire - [Adware][Frostwire]
  > **Note**
  > The interpretation of adware varies a lot from 'ad-supported benign
    software' to 'software that displays or installs ads without user consent'
    and FrostWire's case seems to have been of suggesting other programs
    (_allegedly_ malicious) upon installation, so mostly I'd only recommend
    against it to laypeople. The only ad they currently show you (at least on
    the \*nix client) is to their own Android client when you exit it, see
    https://en.wikipedia.org/wiki/FrostWire#Adware%5Fand%5Fmalware  
  — Gersonzao
- BitLord - Adware
- [Fake 1337x Sites][Fake 1337x Sites]

[VSTorrent]: https://redd.it/x66rz2
[uTorrent]: https://www.theverge.com/2015/3/6/8161251/utorrents-secret-bitcoin-miner-adware-malware
[open source clients]: https://www.reddit.com/r/FREEMEDIAHECKYEAH/wiki/torrent#wiki_.25BA_torrent_clients
[Frostwire]: https://www.virustotal.com/gui/file/6a501792717fd86635d80fb258979b823fd53000c6d683904e2fb2407f1706fd
[Fake 1337x Sites]: https://redd.it/117fq8t

## Software / Apps

- Limewire - Dead for years, anything claiming to be them now should be avoided
- Downloadly (video downloader) - Crypto miner / Note that downloadly.ir is
  unrelated
- Opera (browser) - Poor [privacy practices][Opera privacy practices],
  [link 2][Opera privacy practices, link 2] /
  [Predatory Loan Apps][Opera predatory loan apps]
- McAfee - Preinstalled Bloatware
- Avast - Known for selling user data
- AVG - Owned by Avast
- CCleaner - Owned by Avast, best to use built in win 11 tool or bleachbit
- Private Internet Access - Owned by
  [malware distributor Kape][Kape - Private Internet Access]
- ExpressVPN / ZenMate / CyberGhost - Owned by
  [Kape][Kape - ExpressVPN ZenMate CyberGhost]
- Acord (discord mod) - Has remote eval backdoor
- BlueKik / Bluecord (chat mods) - History of [spam][BlueKik Bluecord - spam] /
  [spying][BlueKik Bluecord - spying]
- Kik (messenger) - App used by mostly
  [predators / scammers][Kik - predators scammers]
- TLauncher (minecraft client) - [Shady][TLauncher - shady] business practices.
  **Note** that [TLauncher Legacy](https://llaun.ch/en) aka Legacy Launcher is
  unrelated and safe.
- PolyMC (minecraft client) - [Owner kicked][PolyMC - owner kicked] all members
  from repo / discord
- GShade (ReShade mod) - the developer added code that can trigger
  [unwanted reboots][GShade - unwanted reboots]
- OnStream - Closed source, possibly [malicious][OnStream]

[Opera privacy practices]: https://www.kuketz-blog.de/opera-datensendeverhalten-desktop-version-browser-check-teil13/
[Opera privacy practices, link 2]: https://rentry.co/operagx
[Opera predatory loan apps]: https://www.androidpolice.com/2020/01/21/opera-predatory-loans/
[Kape - Private Internet Access]: https://redd.it/q3lepv
[Kape - ExpressVPN ZenMate CyberGhost]: https://rentry.co/i8dwr
[BlueKik Bluecord - spam]: https://redd.it/12h2v6n
[BlueKik Bluecord - spying]: https://rentry.co/tvrnw
[Kik - predators scammers]: https://youtu.be/9sPaJxRmIPc
[TLauncher - shady]: https://redd.it/zmzzrt
[PolyMC - owner kicked]: https://redd.it/y6lt6s
[GShade - unwanted reboots]: https://rentry.co/GShade_notice
[OnStream]: https://rentry.co/upo2r

## Fake Z-Lib Sites

<!-- converted from picture -->

```text
These sites are not to be trusted:
zlib . is
zlib . to
zlibrary . to
zlibrary . is
z-lib . is
z-lib . to
```

## Fake Windows Activators

<!-- converted from picture -->

```text
Malwares, don't use

Windows 10 Pro Permanent Activator Ultimate
Windows 10 Permanent Activator Ultimate
Windows Kms Activator Ultimate
Mini KMS Activator Ultimate
Windows 10 Digital License Ultimate
Windows 10 Activator Ultimate
Windows 10 Pro Automatic Permanent Activator
Windows 10 Digital Entitlement Permanent Activator
Office 2016 Permanent Activator Ultimate
Office 2013 Kms Activator Ultimate
Windows 10 Kms Activator Ultimate
Windows Kms Activator Ultimate
Office 2016 Kms Activator Ultimate
Kms Activator Ultimate
Win10actPlus
Windows Vista - 7 - 8 - 8.1 KMS Activator Ultimate
Windows 8.1 KMS Activator Ultimate
Windows 8.1 Product Key Finder Ultimate
Office_WebAct_Plus
Win10.AU
Win10.DLU
Mini.KMS.AU
Office 2019 KMS Activator Ultimate
Microsoft ISO Downloader Premium 2021
KMS.Matrix
KMS Bandit
```
