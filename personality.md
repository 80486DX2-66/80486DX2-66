# About me

| Field | Value |
| ----: | :---- |
| Name | Larry Holst |
| Lifeform | Cybernetic derivative of human |
| Quirks of brain | ADHD-PH, OCPD, HSP of [intellectual type ('the overexcitables')](https://themindsjournal.com/types-of-hsp/) |
| Moral alignment | good lawful |
| Interests | ecology, computer science, electrical engineering; analytical, theoretical, electro-chemistry; F(L)OSS software, mathematics; quantum, classical, nuclear physics; philosophy, creating music and sound effects; all fields of science |
| Political compass | social libertarianism |
| Largest desires | 1. Verified and pure technical knowledge of the world.<br>2. Giving/receiving unconditional love.<br>3. Non-existence of evil. |
