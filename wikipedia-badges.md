# Wikipedia badges

## Beliefs in editing

- This member is in favor of the mandatory use of the letter "ё" in the Russian language
- This member is negative about transliteration
- This member opposes censorship on the Internet
- This member is against advertising on Wikipedia
- This member opposes the use of fair use images on Wikipedia

## Political beliefs

- This member is a secular humanist
- This member is against criminalizing speech on blogs and actions on the Internet
- This member is an advocate of free speech
- This member is a pacifist
- This member is an advocate for the use and development of peaceful nuclear energy

## Language

- This member is negative about transliteration

## Confession

- This member agrees that astrology is a pseudoscience

## Other

- This member accepts any kind of address equally well: "You" and "you", Mr. and Comrade, name and nickname
- This member is an advocate of free software
- This member has no major bad habits

