xxx: You know, FOSS enthusiasts often struggle to admit that sometimes
the open-source alternatives are like a tricycle racing against a Ferrari
in comparison to those snazzy proprietary versions.
xxx: Man, I've been trying to make Vegas dance on Wine, but that turned into
a comedy show. So now I'm stuck with those cruddy FOSS versions pretending
to be Vegas. It's like substitute teacher-level disappointment, y'know?
