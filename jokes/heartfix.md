## Heartfix

```text
C:\>echo I'm so depressed
I'm so depressed

C:\>dir dos\heartfix.exe

 Volume in drive C is YOU
 Volume Serial Number is C5D7-1484
 Directory of C:\DOS

HEARTFIX EXE        16 541 31/05/94    6:22
        1 file(s)         16 541 bytes
                   1 654 615 998 bytes free

C:\>heartfix
Press ENTER to continue...
Now your heart is all fixed!

C:\>
```
