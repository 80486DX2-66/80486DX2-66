                               Software Spectrum
                               -----------------

Rate your software on the scales (1..10):

1. Software Identity:
     1 2 3 4 5 6 7 8 9 10
   [ = = = = = = = = = = ]
   Left: Software as a Service (SaaS)
   Middle: Platform as a Service (PaaS)
   Right: Infrastructure as a Service (IaaS)

2. Software Expression:
     1 2 3 4 5 6 7 8 9 10
   [ = = = = = = = = = = ]
   Left: Native
   Middle: Cross-platform
   Right: Web

3. Software Orientation:
     1 2 3 4 5 6 7 8 9 10
   [ = = = = = = = = = = ]
   Left: Closed-source
   Middle: Open-source
   Right: Free and open-source software (FOSS)

4. Software Drive:
     1 2 3 4 5 6 7 8 9 10
   [ = = = = = = = = = = ]
   Left: Stable
   Middle: Agile
   Right: Kanban

5. Software Desire:
     1 2 3 4 5 6 7 8 9 10
   [ = = = = = = = = = = ]
   Left: Low complexity
   Middle: Medium complexity
   Right: High complexity

6. Software Attitude:
     1 2 3 4 5 6 7 8 9 10
   [ = = = = = = = = = = ]
   Left: Monolithic
   Middle: Micro-service
   Right: Modular

7. Software Exploration:
     1 2 3 4 5 6 7 8 9 10
   [ = = = = = = = = = = ]
   Left: Low risk
   Middle: Moderate risk
   Right: High risk
