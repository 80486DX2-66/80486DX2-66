# 80486DX2-66

Personal repository.

## What you may be interested in / What you came here for
- [Biography](biography.md): Brief description, biography, minimalistic version, extended personality description;
- [Gender and pronouns information, attraction layer cake, sexuality](gender_attraction_sexuality.md): Gender, gender tags, names, pronouns; words: honorifics, person and family descriptions, compliments, relationship descriptions; external links;
- [Personality file](personality.md): Name, lifeform, quirks of brain, moral alignment, interests, politic compass;
- Tier lists
  - [Programming languages](tier-lists/programming-languages.md)
- Configurations
  - [uBlock and compatible custom filter list](config/ublock-filter-list.txt)
- Sheets
  - Slightly [modified post "Untrusted Sites / Software"](sheets/untrusted-sites-and-software.md) from [r/FREEMEDIAHECKYEAH](https://www.reddit.com/r/FREEMEDIAHECKYEAH/comments/10bh0h9/unsafe_sites_software_thread/)
