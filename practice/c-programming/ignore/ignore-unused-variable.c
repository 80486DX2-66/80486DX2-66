/*
 * ignore-unused-variable.c
 *
 * Try compiling this with all warnings enabled:
 *     cc ./ignore-unused-variable.c -Wall -Wextra -Wpedantic
 *
 * Author: Intel A80486DX2-66
 * License: Creative Commons Zero 1.0 Universal or Unlicense
 * SPDX-License-Identifier: CC0-1.0 OR Unlicense
 */

#include <stdlib.h> // for EXIT_SUCCESS

int main(void) {
	int x = 256; // Declare an unused variable with some value
	(void) x; /*
	           * HACK: Suppress compiler warnings about the unused variable by
	           * casting it to `void` type, because `void` type values are
	           * discarded and should be ignored. In fact, `void` is an abstract
	           * type.
	           */

	return EXIT_SUCCESS;
}
