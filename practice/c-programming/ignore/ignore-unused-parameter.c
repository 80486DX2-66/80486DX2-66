/*
 * ignore-unused-parameter.c
 *
 * Try compiling this with all warnings enabled:
 *     cc ./ignore-unused-parameter.c -Wall -Wextra -Wpedantic
 * or specifically:
 *     cc ./ignore-unused-parameter.c -Wunused-parameter
 *
 * If you want the code to emit a warning about the unused parameter, define a
 * macro UNUSED_PARAMETER. Example:
 *     cc ./ignore-unused-parameter.c -Wunused-parameter -DUNUSED_PARAMETER
 *
 * Author: Intel A80486DX2-66
 * License: Creative Commons Zero 1.0 Universal or Unlicense
 * SPDX-License-Identifier: CC0-1.0 OR Unlicense
 */

#include <stdio.h>
#include <stdlib.h>

int main(int argc, char** argv) {
#ifndef UNUSED_PARAMETER
	(void) argc; /*
	              * HACK: Suppress compiler warnings about the unused parameter
	              * by casting it to `void` type, because `void` type values
	              * are discarded and should be ignored. In fact, `void` is an
	              * abstract type.
	              */
#endif

	puts(argv[0]);

	return EXIT_SUCCESS;
}
