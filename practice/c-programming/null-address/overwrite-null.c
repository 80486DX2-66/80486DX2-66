/*
 * overwrite-null.c
 *
 * This approach reads from and writes to NULL address.
 *
 * Author: Intel A80486DX2-66
 * License: Creative Commons Zero 1.0 Universal OR Unlicense
 * SPDX-License-Identifier: CC0-1.0 OR Unlicense
 */

#include <stdlib.h>

#define DISCARD_EXPRESSION(x) ((void) (x))
/*
 * DISCARD_EXPRESSION: see file ignore-unused-parameter.c or
 * ignore-unused-variable.c for an explanation
 */

int main(void) {
	int* x = (int*) NULL, y = *x;
	*x = y;

	DISCARD_EXPRESSION(x);
	DISCARD_EXPRESSION(y);

	return EXIT_SUCCESS;
}
