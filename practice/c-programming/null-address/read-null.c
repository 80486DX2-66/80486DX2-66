/*
 * read-null.c
 *
 * This approach reads from NULL address.
 *
 * Author: Intel A80486DX2-66
 * License: Creative Commons Zero 1.0 Universal OR Unlicense
 * SPDX-License-Identifier: CC0-1.0 OR Unlicense
 */

#include <stdio.h>
#include <stdlib.h>

int main(void) {
	printf("%d\n", *(int*) NULL);

	return EXIT_SUCCESS;
}
