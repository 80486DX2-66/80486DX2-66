/*
 * write-null.c
 *
 * This approach writes to NULL address. Be careful!
 *
 * Author: Intel A80486DX2-66
 * License: Creative Commons Zero 1.0 Universal OR Unlicense
 * SPDX-License-Identifier: CC0-1.0 OR Unlicense
 */

#include <stdio.h>
#include <stdlib.h>

#define DISCARD_EXPRESSION(x) ((void) (x))
/*
 * DISCARD_EXPRESSION: see file ignore-unused-parameter.c or
 * ignore-unused-variable.c for an explanation
 */

int main(void) {
	int* x = (int*) NULL;
	*x = 255;

	DISCARD_EXPRESSION(x);

	return EXIT_SUCCESS;
}
