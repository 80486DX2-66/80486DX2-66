/*
 * always-deallocate.c
 *
 * This library/program serves as an example of how to ensure that all memory is
 * properly deallocated.
 *
 * The allocation functions store all allocated addresses on the stack rather
 * than the heap, providing both stability and ease of implementation.
 *
 * As a bonus, the functions that allocate heap memory either return a valid
 * memory address or terminate the process with the return code `EXIT_FAILURE`.
 * Due to this behavior, if you use other libraries, you will need to write a
 * function to deallocate heap memory allocated by those libraries and bind it
 * using the standard library function `atexit`.
 *
 * Feel free to adjust the configuration in the file `always-deallocate.h`.
 *
 * Author: Intel A80486DX2-66
 * License: Creative Commons Zero 1.0 Universal OR Unlicense
 * SPDX-License-Identifier: CC0-1.0 OR Unlicense
 */

#include "always-deallocate.h"

#ifdef TEST
const char* give_name_to_address(void* address);
#endif

static void* heap_allocated[MAX_ALLOCATED_REGIONS] = {NULL};
static ever_allocated_t ever_allocated = 0;
#if HEAP_ATEXIT_SETUP
static bool heap_setup_finished = false;
#endif

void* heap_alloc(size_t nelem, size_t elsize) {
	HEAP_ALLOC_MACRO(malloc, nelem * elsize);
}

void* heap_alloc_zero(size_t nelem, size_t elsize) {
	HEAP_ALLOC_MACRO(calloc, nelem, elsize);
}

#undef HEAP_ALLOC_MACRO

void heap_dealloc(void* address) {
	if (address == NULL) { // Is it a null pointer?
#ifdef TEST
		puts("[DEBUG] heap_dealloc: NULL is not a valid region");
#endif
		return;
	}

	// Find the address among the managed regions
	for (size_t i = 0; i < MAX_ALLOCATED_REGIONS; ++i) {
		if (address == heap_allocated[i]) {
			free(address);
			heap_allocated[i] = NULL;

#ifdef TEST
			printf("[DEBUG] Successfully deallocated region %s\n",
			       give_name_to_address(address));
#endif

			return;
		}
	}

#ifdef TEST
	printf("[DEBUG] heap_dealloc: %s is not a managed region\n",
	       give_name_to_address(address));
#endif
}

void all_dealloc(void) {
#ifdef TEST
	puts("[DEBUG] all_dealloc called");
#endif

	bool encountered_null = false;

	for (size_t i = 0; i < MAX_ALLOCATED_REGIONS; ++i) {
		// Optimization: If all previous addresses are non-NULL and the count of
		// all allocated addresses is equal to `i`, exit the loop.
		if (encountered_null == false && i == ever_allocated)
			break;

		void* address = heap_allocated[i];
		if (address == NULL)
			encountered_null = true;
		else {
			free(address);

#ifdef TEST
			printf("[DEBUG] Successfully deallocated region %s\n",
			       give_name_to_address(address));
#endif
		}
	}

	ever_allocated = 0;
}

#if HEAP_ATEXIT_SETUP
void heap_setup_function(void) {
	if (atexit(all_dealloc) != 0) {
		perror("[PANIC] Failed to not set up the termination handler for "
		       "deallocation");
		exit(EXIT_FAILURE);
	}

	heap_setup_finished = true;
}

# define heap_setup heap_setup_function
#else
# define heap_setup()
#endif

#ifdef TEST
#include <inttypes.h>
#include <string.h>
#include <time.h>

typedef unsigned char byte;
typedef unsigned char* bytes;

#define STRINGIZE_INTERNAL(x) #x
#define STRINGIZE(x) STRINGIZE_INTERNAL(x)
#define ABSTRACT_PTR_C(value) ((void*) value)

const char* give_name_to_address(void* address) {
	const char* names[] = {
		"Alice Margatroid",
		"Aunn Komano",
		"Aya Shameimaru",
		"Benben Tsukumo",
		"Byakuren Hijiri",
		"Chen",
		"Chimata Tenkyuu",
		"Chiyari Tenkajin",
		"Chiyuri Kitashirakawa",
		"Cirno",
		"Clownpiece",
		"Daiyousei",
		"Doremy Sweet",
		"Eika Ebisu",
		"Eiki Shiki, Yamaxanadu",
		"Eirin Yagokoro",
		"Elly",
		"Enoko Mitsugashira",
		"Eternity Larva",
		"Flandre Scarlet",
		"Fujiwara-no-Mokou",
		"Gengetsu",
		"Goliath",
		"Hata no Kokoro",
		"Hatate Himekaidou",
		"Hecatia Lapislazuli",
		"Hieda no Akyuu",
		"Hina Kagiyama",
		"Hisami Yomotsu",
		"Hong Meiling",
		"Ichirin Kumoi",
		"Iku Nagae",
		"Joon Yorigami",
		"Junko",
		"Kagerou Imaizumi",
		"Kaguya Houraisan",
		"Kana Anaberal",
		"Kanako Yasaka",
		"Kasen Ibaraki",
		"Keiki Haniyasushin",
		"Keine Kamishirasawa",
		"Yachie Kicchou",
		"Kisume",
		"Koakuma",
		"Kogasa Tatara",
		"Koishi Komeiji",
		"Komachi Onozuka",
		"Kongara",
		"Kosuzu Motoori",
		"Kyouko Kasodani",
		"Kutaka Niwatari",
		"Letty Whiterock",
		"Lily White",
		"Louise",
		"Luna Child",
		"Lunasa Prismriver",
		"Lyrica Prismriver",
		"Mai (PC-98)",
		"Mai Teireida",
		"Mamizou Futatsuiwa",
		"Maribel Hearn",
		"Marisa Kirisame",
		"Mayumi Joutouguu",
		"Medicine Melancholy",
		"Megumu Iizunamaru",
		"Meira",
		"Merlin Prismriver",
		"Mike Goutokuji",
		"Mima",
		"Minamitsu Murasa",
		"Minoriko Aki",
		"Misumaru Tamatsukuri",
		"Miyoi Okunoda",
		"Momiji Inubashiri",
		"Momoyo Himemushi",
		"Mononobe-no-Futo",
		"Mugetsu",
		"Mystia Lorelei",
		"Narumi Yatadera",
		"Nazrin",
		"Nemuno Sakata",
		"Nitori Kawashiro",
		"Nue Houjuu",
		"Okina Matara",
		"Parsee Mizuhashi",
		"Patchouli Knowledge",
		"Raiko Horikawa",
		"Ran Yakumo",
		"Reimu Hakurei",
		"Reisen Udongein Inaba",
		"Reisen",
		"Remilia Scarlet",
		"Renko Usami",
		"Rin Kaenbyou",
		"Rin Satsuki",
		"Ringo",
		"Rinnosuke Morichika",
		"Rumia",
		"Ruukoto",
		"Sagume Kishin",
		"Saki Kurokoma",
		"Sakuya Izayoi",
		"Sanae Kochiya",
		"Sannyo Komakusa",
		"Sara",
		"Sariel",
		"Satono Nishida",
		"Satori Komeiji",
		"Seiga Kaku",
		"Seija Kijin",
		"Seiran",
		"Sekibanki",
		"Shinki",
		"Shinmyoumaru Sukuna",
		"Shion Yorigami",
		"Shizuha Aki",
		"Shou Toramaru",
		"Soga-no-Tojiko",
		"Son Biten",
		"Star Sapphire",
		"Suika Ibuki",
		"Sumireko Usami",
		"Sunny Milk",
		"Suwako Moriya",
		"Takane Yamashiro",
		"Tenshi Hinanawi",
		"Tewi Inaba",
		"Toyosatomimi-no-Miko",
		"Tsukasa Kudamaki",
		"Urumi Ushizaki",
		"Utsuho Reiuji",
		"Wakasagihime",
		"Watatsuki-no-Toyohime",
		"Watatsuki-no-Yorihime",
		"Wriggle Nightbug",
		"Yamame Kurodani",
		"Yatsuhashi Tsukumo",
		"Yoshika Miyako",
		"Youmu Konpaku",
		"Yukari Yakumo",
		"Yuki",
		"Yumemi Okazaki",
		"Yuugi Hoshiguma",
		"Yuuka Kazami",
		"Yuuma Toutetsu",
		"Yuyuko Saigyouji",
		"Zanmu Nippaku"
	};

	return names[(uintptr_t) address % (sizeof names / sizeof(char*))];
}

int main(void) {
#if HEAP_ATEXIT_SETUP
	/* XXX: mandatory function call */ heap_setup();
#endif

	puts("Allocating 1 byte");
	bytes one_byte = heap_alloc(1, sizeof(byte));
	printf("\taddress = %s\n", give_name_to_address(one_byte));

	puts("Allocating space for a ctime string");
	char* string = heap_alloc(64, sizeof(byte));
	printf("\taddress = %s\n", give_name_to_address(string));
	snprintf(string, 64, "The time is %s", ctime(&(time_t){time(NULL)}));
	string[strcspn(string, "\n")] = '\0';
	puts(string);

	heap_dealloc(NULL);

	heap_dealloc(ABSTRACT_PTR_C(0xdeadbeb5));

	char* ascii_text_buffer = malloc(sizeof(char));
	if (ascii_text_buffer == NULL) {
		perror("malloc");
		exit(EXIT_FAILURE);
	}

	puts("Allocating " STRINGIZE(SIZE_MAX) " bytes");
	bytes n_bytes = heap_alloc(SIZE_MAX, sizeof(byte));
	printf("\taddress = %s\n", give_name_to_address(n_bytes));

	return EXIT_SUCCESS;
}
#endif
