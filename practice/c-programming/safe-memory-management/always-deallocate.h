/*
 * always-deallocate.h
 *
 * Read the description in the file `always-deallocate.c` for more information.
 *
 * Author: Intel A80486DX2-66
 * License: Creative Commons Zero 1.0 Universal OR Unlicense
 * SPDX-License-Identifier: CC0-1.0 OR Unlicense
 */

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#define HEAP_ATEXIT_SETUP 1 /* Feel free to change this value to 0 if you don't
                             * want the library to register the function
                             * `all_dealloc` with `atexit`.
                             *
                             * In that case, you will need to ensure that the
                             * function `all_dealloc` is called manually before
                             * your program exits!
                             */

#define MAX_ALLOCATED_REGIONS 16ULL /* feel free to adjust the value */
typedef uint8_t ever_allocated_t; /* Please change this type to one that can
                                   * accommodate the value of
                                   * `MAX_ALLOCATED_REGIONS`.
                                   */

#if HEAP_ATEXIT_SETUP
# define HEAP_FINISH   \
    exit(EXIT_FAILURE)
#else
# define HEAP_FINISH   \
    all_dealloc();     \
    exit(EXIT_FAILURE)
#endif

#if HEAP_ATEXIT_SETUP
# define HEAP_SETUP_CHECK() do {                                              \
    if (heap_setup_finished == false) {                                       \
       perror("[Internal Error] The function `heap_setup()` was not called"); \
       HEAP_FINISH;                                                           \
    }                                                                         \
} while (0)
#else
# define HEAP_SETUP_CHECK()
#endif

#define HEAP_ALLOC_MACRO(function, ...) do {             \
    HEAP_SETUP_CHECK();                                  \
                                                         \
    void* address = function(__VA_ARGS__);               \
                                                         \
    if (address == NULL) {                               \
        perror("[PANIC] heap memory allocation failed"); \
        HEAP_FINISH;                                     \
    }                                                    \
                                                         \
    heap_allocated[ever_allocated++] = address;          \
    return address;                                      \
} while (0)

void* heap_alloc(size_t nelem, size_t elsize);
void* heap_alloc_zero(size_t nelem, size_t elsize);
void heap_dealloc(void* address);
void all_dealloc(void);
#if HEAP_ATEXIT_SETUP
void heap_setup_function(void);
#endif
