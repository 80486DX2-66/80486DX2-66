#!/bin/sh

error() {
	printf -- '%s\n' "$*" >&2
	exit 1
}

if [ -z "$1" ] || [ -z "$2" ]; then
	cat << EOF
${0##*/} <image> <audio> [--animated]

Use the parameter --animated if the image is either GIF or APNG
EOF
	return 0
fi

# check if the specified files exist
if [ ! -f "$1" ]; then
	error "$1 does not exist"
elif [ ! -f "$2" ]; then
	error "$2 does not exist"
fi

[ -d "./build" ] || mkdir ./build

if [ "$3" = "--animated" ]; then
	ffmpeg -stream_loop -1 -i "$1" -i "$2" -map_metadata -1 -c:a libmp3lame \
		-ar 44100 -ac 2 -b:a 256k -c:v libx264 -crf 15 -movflags +faststart \
		-shortest ./build/output.mp4
else
	ffmpeg -loop 1 -i "$1" -i "$2" -map_metadata -1 -c:a libmp3lame -ar 44100 \
		-ac 2 -b:a 192k -c:v libx264 -r 20 -crf 10 -movflags +faststart \
		-shortest ./build/output.mp4
fi

return "$?"
