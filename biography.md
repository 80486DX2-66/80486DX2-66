# Larry "80486DX2-66" Holst

## Classic

> a cyborg with emotions feature

A cyborg, originally from Bristol, UK, currently resides in Russia. He has a diverse range of interests including audio production and music, computer hardware, electrical engineering, philosophy, mathematics, software development, quantum physics, and chemistry. Larry is particularly passionate about programming in x86 Assembly for IBM PC/AT in the absence of an environment and in DOS; Bash; C; D; Python 3. He is a Linux intermediate level user and also cares about the planet's environment and is a staunch advocate of free, free and open source software. Larry also enjoys creating art and releasing it under Creative Commons licenses.

## Minimalistic

In a nutshell, an English cyborg.

**Interests**:
- Audio production and music
- Computer hardware
- Electrical engineering
- Philosophy
- Mathematics
- Software development
- Quantum physics
- Chemistry

**Personality**: open-minded, curious, and always willing to learn; a kind and supportive friend who offers science-based advice to those in need; committed to maintaining a healthy lifestyle and is interested in fitness, nutrition, and wellness.

**Programming Languages**: *not sure*

**Operating System Preference**: Despite being new to OpenBSD, Larry has already developed a strong interest in the operating system and its culture, and is excited to explore it further.

**Advocacy**: cares deeply about the planet's environment and is a staunch advocate of open source, free, and libre software.

**Artistic Pursuits**: enjoys creating music and releasing it under permissive Creative Commons licenses.

**Links**:
- [Pronouns.page cards](https://pronouns.page/@80486DX2-66) -- available in English, French, German, Russian, Spanish, Ukrainian languages;
- A README.md in self-repository on [Disroot's Forgejo instance](https://git.disroot.org/80486DX2-66/80486DX2-66/src/branch/main/README.md) / [GitLab](https://gitlab.com/80486DX2-66/storage/-/blob/main/README.md) with a bit more information.

**Powered by**: ECS SL-486E

## Extended personality description

I'm:
- soft inside
- easily offended
- a logical thinker
- cool & collected
- blunt with opinions
- energetic type
- excitable
- a motivator
- often beaming
- highly optimistic
- mysterious
- chaste & modest
- in touch with "supernatural" (I use cognitive-behavioral therapy to help)
- a fast learner
- reliable & efficient.

I live a peaceful life. I have "mega knowledge".

### Alternative representation

- a fun lover
- a quick apologizer
- a steady and faithful friend
- accepting
- affectionate
- ambitious
- an easy friend maker
- an excessive planner
- argumentative
- cautious
- charismatic
- confident
- considerate
- content with themselves
- curious
- detailed
- difficult to please
- diplomatic
- efficient
- enthusiastic
- focused
- good at planning
- good at preventing problems
- good at problem solving
- great in an emergency
- highly creative in poetry, art and invention
- independent
- kind
- lively
- motivating
- not interested in following through with tasks that are boring
- observant
- optimistic
- organized
- outgoing
- passionate
- peacemaking
- perfectionistic
- permissive
- pleasant
- practical
- prone to laziness
- quiet and calm
- rational
- relaxed
- right in the vast majority of cases
- schedule and goal oriented
- sociable
- sometimes shy
- spontaneous
- stubborn
- thoughtful
- too cautious
- warm-hearted

## Test results

- https://www.arealme.com/what-chemical-element-are-you/en/
  > You are very outgoing and never like to throw in the towel on anything. You rigorously pursue the truth. You are like thorium.
